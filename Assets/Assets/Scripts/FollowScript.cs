﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowScript : MonoBehaviour
{

    public float lookRange;

    public Transform target;
    NavMeshAgent agent;
    private Vector3 startPos;
    public bool playerHiding = false;
    public LayerMask wall;
    public GameObject shooter;
    private bool lureGuard;
    private Vector3 lurePos;


    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        agent = GetComponent<NavMeshAgent>();
        int speed = 3;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        //Lure
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            lureGuard = true;
            agent.SetDestination(target.position);
            lurePos = target.position;
        }
        if (Vector3.Distance(lurePos, transform.position) < 6) //6 is used as that is the stopping distance of the agent
        {
            lureGuard = false;
        }

        if (distance <= lookRange && (!playerHiding && !BehindWall()))
        {
            agent.SetDestination(target.position);

            if(distance < agent.stoppingDistance)
            {
                //Face the player 
                FaceTarget();
                if (shooter != null)
                {
                    shooter.GetComponent<EnemyShootArrow>().enabled = false;
                }
            }
        }
        if((distance > lookRange || playerHiding) && !lureGuard)
        {
            agent.SetDestination(startPos);
        }
    }

    public bool BehindWall()
    {
        if (target != null)
        {
            if (Physics.Linecast(transform.position, target.position, wall))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

    }

    private void OnDrawGizmosSelected()
    {
        if (BehindWall())
        {
            Gizmos.color = Color.blue;
        }
        else
        {
            Gizmos.color = Color.red;
        }
        Gizmos.DrawWireSphere(transform.position, lookRange);
    }


}
