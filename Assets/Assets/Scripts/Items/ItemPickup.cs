﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : Interactable
{
    public Item item;
    // Start is called before the first frame update
    public  override void Interact()
    {
        base.Interact();
        PickUp();
    }
  public virtual void PickUp()
    {
        Debug.Log("picking up an item" + item.name);
        Inventory.instance.Add(item);

        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
