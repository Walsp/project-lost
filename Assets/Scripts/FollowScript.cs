﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowScript : MonoBehaviour
{

    public float lookRange = 10;

    public Transform target;
    NavMeshAgent agent;
    private Vector3 startPos;
    public bool playerHiding = false;


    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        agent = GetComponent<NavMeshAgent>();
        target = PlayerManager.instance.player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if(distance <= lookRange && playerHiding == false)
        {
            agent.SetDestination(target.position);
            if(distance < agent.stoppingDistance)
            {
                //Face the player 
                FaceTarget();
            }
        }
        else if(distance <= lookRange && playerHiding == true)
        {
            agent.SetDestination(startPos);
        }
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRange);
    }


}
