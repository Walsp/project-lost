﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootArrow : MonoBehaviour
{
    public GameObject arrowPrefab;
    public GameObject arrow;
    public GameObject bow;
    public float pullSpeed = 50;
    int numArrows = 10;
    float pullAmount = 0;
    bool arrowSlotted = false;
    private float timer = 0;
   // public Transform playerCam;
    // Start is called before the first frame update
    void Start()
    {
        SpawnArrow();
        //playerCam = GameObject.FindGameObjectWithTag("Hand").transform;
    }

    // Update is called once per frame
    void Update()
    {
        ShootLogic();
    }

    void SpawnArrow()
    {
        if(numArrows > 0)
        {
            arrowSlotted = true;
            arrow = Instantiate(arrowPrefab, transform.position, transform.rotation) as GameObject;
            arrow.transform.parent = transform;
        }
    }
    void ShootLogic()
    {
        if(numArrows > 0)
        {
            if (pullAmount > 100)
                pullAmount = 100;

            SkinnedMeshRenderer bowSkin = bow.transform.GetComponent<SkinnedMeshRenderer>();
            SkinnedMeshRenderer arrowSkin = arrow.transform.GetComponent<SkinnedMeshRenderer>();
            Rigidbody arrowRB = arrow.transform.GetComponent<Rigidbody>();
            ProjectileAddForce arrowProjectile = arrow.transform.GetComponent<ProjectileAddForce>();
            if (Input.GetMouseButton(0))
                {
                    pullAmount += Time.deltaTime * pullSpeed;
                }
                if (Input.GetMouseButtonUp(0))
                {
                    
                    arrowSlotted = false;
                    arrowRB.isKinematic = false;
                    arrow.transform.parent = null;
                    numArrows--;
                arrowProjectile.shootForce = arrowProjectile.shootForce * ((pullAmount / 100) + 0.05f);
                pullAmount = 0;
                arrowProjectile.enabled = true;
               //// Debug.Log("HOERAAA");
                }
            //bowSkin.SetBlendShapeWeight(0, pullAmount);
            //arrowSkin.SetBlendShapeWeight(0, pullAmount);
                if (Input.GetMouseButtonDown(0) && arrowSlotted == false)
                {
                    SpawnArrow();
                }


        }
    }
}
