﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObject : MonoBehaviour
{
    public Transform player;
    public Transform playerCam;
    bool inRange = false;
    public float throwForce = 1000;
    public Camera camera;
    public float dist;
    Rigidbody rb;
    bool beingCarried = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(gameObject.transform.position, player.position);
        if (dist <= 5f)
        {
            inRange = true;
        }
        else
        {
            inRange = false;
        }
        //Picks up all objects in range
        if (inRange && Input.GetButtonDown("Use") && beingCarried == false)
        {
            Pickup();
        }
        if(beingCarried == true && Input.GetMouseButtonDown(1))
        {
            GetComponent<Rigidbody>().isKinematic = false;
            transform.parent = null;
            beingCarried = false;
        } 
    }

    void Pickup()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(GameObject.FindGameObjectWithTag("Crosshair").transform.position);

        if (Physics.Raycast(ray, out hit, dist))
        {
            if (hit.collider.tag == "PickUpObject")
            {
                GetComponent<Rigidbody>().isKinematic = true;
                transform.parent = playerCam;
                transform.position = playerCam.position;
                transform.rotation = playerCam.rotation;
                beingCarried = true;
            }

        }
    }
}
