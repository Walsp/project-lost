﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThrowableObject : ItemPickup
{
    public Transform player;
    public Transform playerCam;
    bool inRange = false;
    bool beingCarried = false;
    public int damage;
    private float mouse;
    public float throwForce = 1000;
    private float dist;
    Rigidbody rb;
    private GameObject crossHair;
    private bool released;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

   
    // Update is called once per frame
    void Update()
    {
      /*  dist = Vector3.Distance(gameObject.transform.position, player.position);
        if (dist <= 5f)
        {
            inRange = true;
        }
        else
        {
            inRange = false;
        }
        //Picks up all objects in range
        if (inRange && Input.GetButtonDown("Use") )
        {
            Pickup();
        } *////////////////////////
        }

    void FixedUpdate()
    {
        //If the object is moving and has been released, rotate the object downwards
        if (rb.velocity != Vector3.zero && released == true)
        {
            rb.rotation = Quaternion.LookRotation(rb.velocity.normalized);
        }
        ThrowObject();

    }

    public void ThrowObject()
    {
        if (beingCarried)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rb.isKinematic = false;
                transform.parent = null;
                //Makes the object shoot forwards
                rb.AddForce(gameObject.transform.forward * throwForce);
                beingCarried = false;
                released = true;
                //Vector3.Slerp(gameObject.transform.forward, rb.velocity.normalized, Time.deltaTime * 2);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                rb.isKinematic = false;
                transform.parent = null;
                beingCarried = false;
            }
        }
    }

  public override void PickUp()
    {
        // RaycastHit hit;
        //Shoots a ray forward from cam's position and checks if it hits something
        //   if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 100))
        // {
        //     if (hit.collider.tag == "PickUpObject")
        //    {
        //Checks if the player already has a child object (weapon)
        //     if (playerCam.transform.childCount == 0)
        //   {
        Inventory.instance.Add(item);
        GetComponent<Rigidbody>().isKinematic = true;
                    transform.parent = playerCam;
                    transform.position = playerCam.position;
                    transform.rotation = playerCam.rotation;
                    beingCarried = true;
                    released = false;
        Debug.Log("us this doing anything??");
              //  }
        //    }

       // }
    }

    private void OnCollisionEnter(Collision other)
    {
        //Makes the object stick in the ground
         rb.isKinematic = true;
    }
}