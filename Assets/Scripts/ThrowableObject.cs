﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThrowableObject : MonoBehaviour
{
    public Transform player;
    public Transform playerCam;
    bool inRange = false;
    bool beingCarried = false;
    public int damage;
    private float mouse;
    public float throwForce = 1000;
    public Camera camera;
    public float dist;
    Rigidbody rb;



    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(gameObject.transform.position, player.position);
        if (dist <= 5f)
        {
            inRange = true;
        }
        else
        {
            inRange = false;
        }
        //Picks up all objects in range
        if (inRange && Input.GetButtonDown("Use") )
        {
            Pickup();
        }
        ThrowObject();
    }

    public void ApplyGravity()
    {
        float yVelocity = rb.velocity.y;
        float xVelocity = rb.velocity.x;
        float zVelocity = rb.velocity.z;

        //Calculate the angle in which the object should fall down
        float combinedVelocity = Mathf.Sqrt(xVelocity * xVelocity + zVelocity * zVelocity);
        float fallAngle = -1 * Mathf.Atan2(yVelocity, combinedVelocity) * 180 / Mathf.PI;

        //Apply the angle
        transform.eulerAngles = new Vector3(fallAngle, transform.eulerAngles.y, transform.eulerAngles.z);
    }

    public void ThrowObject()
    {
        if (beingCarried)
        {
            if (Input.GetMouseButtonDown(0))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;

                GetComponent<Rigidbody>().AddForce(playerCam.forward * throwForce);
                ApplyGravity();
            }
            else if (Input.GetMouseButtonDown(1))
            {
                GetComponent<Rigidbody>().isKinematic = false;
                transform.parent = null;
                beingCarried = false;
            }
        }
    }

    void Pickup()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(GameObject.FindGameObjectWithTag("Crosshair").transform.position);

        if (Physics.Raycast(ray, out hit, dist))
        {
            if (hit.collider.tag == "PickUpObject")
            {
                GetComponent<Rigidbody>().isKinematic = true;
                transform.parent = playerCam;
                transform.position = playerCam.position;
                transform.rotation = playerCam.rotation;
                beingCarried = true;
            }

        }
    }
}