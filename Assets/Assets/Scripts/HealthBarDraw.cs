﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarDrawPlayer : MonoBehaviour
{

    public Image foregroundImage;
    private float updateSpeedSeconds = 0.5f;
    private GameObject player;


    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<HealthBarPlayer>().OnHealthPctChanged += HandleHealthChanged;
    }

    private void HandleHealthChanged(float pct)
    {
        StartCoroutine(ChangeToPct(pct));
    }

    private IEnumerator ChangeToPct(float pct)
    {
        float preChangePct = foregroundImage.fillAmount;
        float elapsed = 0f;

        while (elapsed < updateSpeedSeconds)
        {
            elapsed += Time.deltaTime;
            foregroundImage.fillAmount = Mathf.Lerp(preChangePct, pct, elapsed / updateSpeedSeconds);
 
            yield return null;
        }

      //  foregroundImage.fillAmount = pct * 100;

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
