﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectingPoint : MonoBehaviour
{
    public int totalObjects = 0;
    public int objectsFound = 0;
    public Text scoreText;

    private void Update()
    {
        scoreText.text = "Objects found:" + objectsFound + "/" + totalObjects;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PickUpObject"))
        {
            //Destroy(other.gameObject);
            objectsFound++;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PickUpObject"))
        {
            //Decrease Score
            objectsFound--;
        }
    }
}
