﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileAddForce : MonoBehaviour
{
    Rigidbody rb;
    public float shootForce = 1000;

    // Start is called before the first frame update
    void OnEnable()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        ApplyForce();
    }

    // Update is called once per frame
    void Update()
    {
        ApplyGravity();
    }

    void ApplyForce()
    {
        rb.AddRelativeForce(Vector3.forward * shootForce);
    }

    void ApplyGravity()
    {
        float yVelocity = rb.velocity.y;
        float xVelocity = rb.velocity.x;
        float zVelocity = rb.velocity.z;

        //Calculate the angle in which the object should fall down
        float combinedVelocity = Mathf.Sqrt(xVelocity * xVelocity + zVelocity * zVelocity);
        float fallAngle = -1 * Mathf.Atan2(yVelocity, combinedVelocity) * 180 / Mathf.PI;

        //Apply the angle
        transform.eulerAngles = new Vector3(fallAngle, transform.eulerAngles.y, transform.eulerAngles.z);
    }
}
