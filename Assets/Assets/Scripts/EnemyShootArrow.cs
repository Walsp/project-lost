﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootArrow : MonoBehaviour
{
    public GameObject arrowPrefab;
    public GameObject arrow;
    public GameObject bow;
    public float pullSpeed = 50;
    int numArrows = 10;
    float pullAmount = 0;
    bool arrowSlotted = false;
    private float timer = 0;

    public float maxTime = 5;
    public float minTime = 2;

    //current time
    private float time;

    //The time to spawn the object
    private float spawnTime;
    // public Transform playerCam;
    // Start is called before the first frame update
    private bool shoot;
    // == enable dit script wanneer de enemy de player ziet //
    void Start()
    {
        SpawnArrow();
        //playerCam = GameObject.FindGameObjectWithTag("Hand").transform;
        SetRandomTime();
        time = minTime;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ShootLogic();
    }

    void SpawnArrow()
    {
        if (numArrows > 0)
        {
            arrowSlotted = true;
            arrow = Instantiate(arrowPrefab, transform.position, transform.rotation) as GameObject;
            arrow.transform.parent = transform;
        }
    }
    void SetRandomTime()
    {
        spawnTime = Random.Range(minTime, maxTime);
    }

    void SpawnObject()
    {
        time = minTime;
        shoot = true;
        //Instantiate(spawnObject, transform.position, spawnObject.transform.rotation);
    }
    void ShootLogic()
    {
        //Counts up
        time += Time.deltaTime;

        if (time >= spawnTime)
        {
            SpawnObject();
        }

        if (shoot) { 

        
            if (numArrows > 0)
            {
              

                if (pullAmount > 100)
                    pullAmount = 100;
             

                SkinnedMeshRenderer bowSkin = bow.transform.GetComponent<SkinnedMeshRenderer>();
                SkinnedMeshRenderer arrowSkin = arrow.transform.GetComponent<SkinnedMeshRenderer>();
                Rigidbody arrowRB = arrow.transform.GetComponent<Rigidbody>();
                ProjectileAddForce arrowProjectile = arrow.transform.GetComponent<ProjectileAddForce>();


                if (shoot)
                {
                    pullAmount = Random.Range(40, 70); 
              

                }

                if (shoot)
                {

                    arrowSlotted = false;
                    arrowRB.isKinematic = false;
                    arrow.transform.parent = null;
                    numArrows--;
                    arrowProjectile.shootForce = arrowProjectile.shootForce * ((pullAmount / 100) + 0.05f);

                    arrowProjectile.enabled = true;
                    shoot = false;
                    //// Debug.Log("HOERAAA");
                }
                //bowSkin.SetBlendShapeWeight(0, pullAmount);
                //arrowSkin.SetBlendShapeWeight(0, pullAmount);
                if (arrowSlotted == false)
                {
                    SpawnArrow();
                }
            } 

            }
        }
    
}
