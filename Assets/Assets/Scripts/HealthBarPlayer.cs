﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthBarPlayer : MonoBehaviour
{
    [SerializeField]
    private  float maxHealth;
    private float health;
    public event Action<float> OnHealthPctChanged = delegate { };
    public GameObject mainObject;
    public Transform spawnPosition;
    // Start is called before the first frame update
   private void Start()
    {
        maxHealth = 30;
        health = maxHealth;
    }

    private void DealDamage(int damage )
    {
        health -= damage;
        float currentHealthPct = health / maxHealth;
        OnHealthPctChanged(currentHealthPct);
     if (currentHealthPct <= -0)
        {
           
            SceneManager.LoadScene("Scene 1");
        } 
    }


    private void heal(int damage)
    {
        health -= damage;
        float currentHealthPct = health / maxHealth;
        OnHealthPctChanged(currentHealthPct);
        if (currentHealthPct <= -0)
        {

            SceneManager.LoadScene("Scene 1");
        }
    }

    private void Die()
    {
        transform.position = new Vector3(spawnPosition.position.x, spawnPosition.position.y, spawnPosition.position.z);
        // lose items? Destroy(mainObject);

    }

  void OnTriggerEnter(Collider collide)
    {
        if (collide.gameObject.CompareTag("Arrow"))
        {
            DealDamage(3);
           
        }

        if (collide.transform.tag == "Spear")
        {
            DealDamage(5);
        }
    }










    void OnTriggerExit(Collider collide)
    {

        if (collide.gameObject.CompareTag("Arrow"))
        {
       
        }
        if (collide.transform.tag == "Arrow")
        {
            
            

        }

        if (collide.transform.tag == "Spear")
        {
            DealDamage(2);



        }
    }

}
