﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGrass : MonoBehaviour
{
    private FollowScript enemy;
    private Transform player;

    private void Update()
    {
        enemy = FindObjectOfType<FollowScript>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemy.playerHiding = true;
            Debug.Log("Geen target");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemy.playerHiding = false;
        }
    }
}
